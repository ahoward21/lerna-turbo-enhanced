[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

# Lerna Turbo Template

A template monorepo with Turborepo, Lerna, and Gitlab CI/CD.

Read the in-depth blog post
[Lerna and Turborepo with Gitlab CI/CD 📖](https://www.delightfulengineering.com/blog/lerna-turbo-gitlab).

## Tools

- [Turborepo](https://turborepo.org/) for task running.
- [Lerna](https://lerna.js.org/) for versioning and publishing.
- [Husky](https://typicode.github.io/husky/#/) for git hooks.
- [Commitizen](https://github.com/commitizen/cz-cli) for its CLI and
  conventional changelog adapter.
- [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/) for CI/CD workflow.

## Features

- Extendable Typescript configuration.
- Extendable Jest configuration.
- Trunk-based workflow with fully automated CI/CD.
  - Canary publishing on **working** branches (nightly builds).
  - Major / Minor / Patch versioning, tag and changelog generation on **main**
    branch. Version bumps and changelogs are automatically committed back to
    main branch.
  - Husky pre-commit-msg hooks triggers interactive Committizen _cz-cli_ to
    enforce conventional commits when running `git commit`.

## Usage Requirements

- Yarn v1: `npm install -g yarn`.
- The following CI/CD variables are required in Gitlab:
  - `GL_TOKEN` variable with API access and read/write permissions on the
    repository. This is required for Lerna to automatically commit package.json
    version bump and changelogs back to the main branch after versioning and
    publishing.
  - `NPM_TOKEN` with automation permissions.

## Strategy

![architecture diagram](./images/architecture.png)

## Tips

- Don't squash commits on merging feature branches to main branch, the changelog
  will not reflect the changes that were made.
- When updating multiple packages at once on a feature branch, if you want
  canary publishes for all updated packages, you will need to push committed
  changes related to each package separately as canary publishing will refer to
  most recent commit on a feature branch to determine which packages to publish.
- Avoid long running feature branches. Commit and merge changes back to **main**
  as often as possible.
- Work with two branches only, **main** and **working** branches. If bugs are
  found, simply use a **working** branch and merge fixes to main.
