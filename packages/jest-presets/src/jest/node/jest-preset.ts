import type { Config } from "jest";

export const NodeConfig: Config = {
  transform: {
    "^.+\\.ts$": "ts-jest",
  },
  verbose: true,
  preset: "ts-jest",
  collectCoverage: true,
};
