import type { Config } from "jest";
import { NodeConfig } from "@ahowardtech/jest-presets";

const config: Config = {
  ...NodeConfig,
};

export default config;
