#!/bin/bash
apt-get update
apt-get install -y jq
DOES_RELEASE_BRANCH_EXIST=`git ls-remote --heads origin $RELEASE_BRANCH | wc -l`

if [ "$DOES_RELEASE_BRANCH_EXIST" = 1 ]; then
    echo "Release branch already exists. Pushing commits there."
    git pull
    git checkout $RELEASE_BRANCH
    git pull origin $RELEASE_BRANCH
    pnpm lerna version --allow-branch $RELEASE_BRANCH --no-push --yes
    git push origin $RELEASE_BRANCH
    exit 0
fi

if [ "$DOES_RELEASE_BRANCH_EXIST" = 0 ]; then
    echo "Release branch does not exist. Creating it."
    git checkout -b $RELEASE_BRANCH
    pnpm lerna version --allow-branch $RELEASE_BRANCH --no-push --yes
    git push origin $RELEASE_BRANCH
    set -e

    if [ -z "$GL_TOKEN" ]; then
      echo "GL_TOKEN not set"
      echo "Please set the GitLab Private Token as GL_TOKEN"
      exit 1
    fi

    # Extract the host where the server is running, and add the URL to the APIs
    [[ $CI_PROJECT_URL =~ ^https?://[^/]+ ]] && HOST="${BASH_REMATCH[0]}/api/v4/projects/"

    # Look which is the default branch
    TARGET_BRANCH=`curl --silent "${HOST}${CI_PROJECT_ID}" --header "PRIVATE-TOKEN:${GL_TOKEN}" | jq --raw-output '.default_branch'`;

    # The description of our new MR, we want to remove the branch after the MR has
    # been closed
    BODY="{
        \"id\": ${CI_PROJECT_ID},
        \"source_branch\": \"${CI_COMMIT_REF_NAME}\",
        \"target_branch\": \"${TARGET_BRANCH}\",
        \"remove_source_branch\": true,
        \"title\": \"WIP: ${CI_COMMIT_REF_NAME}\",
        \"assignee_id\":\"${GITLAB_USER_ID}\"
    }";

    # Require a list of all the merge request and take a look if there is already
    # one with the same source branch
    LISTMR=`curl --silent "${HOST}${CI_PROJECT_ID}/merge_requests?state=opened" --header "PRIVATE-TOKEN:${GL_TOKEN}"`;
    COUNTBRANCHES=`echo ${LISTMR} | grep -o "\"source_branch\":\"${CI_COMMIT_REF_NAME}\"" | wc -l`;

    # No MR found, let's create a new one
    if [ ${COUNTBRANCHES} -eq "0" ]; then
        curl -X POST "${HOST}${CI_PROJECT_ID}/merge_requests" \
            --header "PRIVATE-TOKEN:${GL_TOKEN}" \
            --header "Content-Type: application/json" \
            --data "${BODY}";

        echo "Opened a new merge request: RELEASE: ${CI_COMMIT_REF_NAME} and assigned to you";
        exit;
    fi

    echo "No new merge request opened";

    exit 0
fi